// app.js
/*
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.send('Hello, DevOps World!');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
*/

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.get('/welcome', (req, res) => {
  res.send('Welcome to DevOps World!');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

module.exports = app;

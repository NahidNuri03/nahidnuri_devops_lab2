# DevOps Lab 2: CI/CD Pipeline with GitLab

This repository contains the code and configuration for my simple Node.js application and a CI/CD pipeline using GitLab.

## Table of Contents
- [Prerequisites](#prerequisites)
- [Project Structure](#project-structure)
- [Setting up the CI/CD Pipeline](#setting-up-the-cicd-pipeline)
- [Building the Docker Image](#building-the-docker-image)
- [Running the Application Locally](#running-the-application-locally)


## Prerequisites

Before getting started, you should have the following prerequisites installed on your local machine:

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/)
- [Docker](https://www.docker.com/)

## Project Structure

My structure is as follows:

- app.js # Node.js application
- Dockerfile # Dockerfile for building the Docker image
- package.json # Node.js dependencies
- test.js # Mocha test script
- .gitlab-ci.yml # GitLab CI/CD configuration, etc.

## Setting up the CI/CD Pipeline

1. Fork or clone this repository to your own GitLab account.
2. Configure a custom Windows runner or use GitLab's shared runners.
3. Set up a GitLab repository for this project.
4. Create a `.gitlab-ci.yml` file with the CI/CD pipeline configuration, which includes build, test, and deploy stages.

## Building the Docker Image

To build the Docker image of the application, follow these steps: 

1. Open your command line or terminal.
2. Navigate to the root directory of the project.
3. Run the following command:

```bash
docker build -t nahidnuri_devops_lab2 . 
```

This command will build a Docker image with the name nahidnuri_devops_lab2

## Running the Application Locally

To run the application locally, use the following command:

```bash
docker run -p 3000:3000 nahidnuri_devops_lab2
```
The application will be accessible at http://localhost:3000.



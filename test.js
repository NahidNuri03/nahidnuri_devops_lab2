const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('./app');

const expect = chai.expect;

chai.use(chaiHttp);

describe('GET /welcome', function () {
  it('should respond with "Welcome to DevOps World!"', function (done) {
    chai
      .request(app)
      .get('/welcome')
      .end(function (err, res) {
        expect(res).to.have.status(200);
        expect(res.text).to.equal('Welcome to DevOps World!');
        done();
      });
  });
});
